/**
 * Created by gomes on 03/10/16.
 */
if (process.env.NODE_ENV === 'dev') {
    module.exports = require('./configureStore.dev')
} else {
    module.exports = require('./configureStore.prod')
}