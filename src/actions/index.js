/**
 * Created by gomes on 03/10/16.
 */
export const ATTEMPTING_LOGIN = 'ATTEMPTING_LOGIN';
export const LOGOUT = 'LOGOUT';
export const LOGIN = 'LOGIN';
export const REGISTER = 'REGISTER';
export const DISPLAY_FEEDBACK_MESSAGE = 'DISPLAY_FEEDBACK_MESSAGE';
export const DISMISS_FEEDBACK_MESSAGE = 'DISMISS_FEEDBACK_MESSAGE';
export const DISMISS_ALL_FEEDBACK_MESSAGE = 'DISMISS_ALL_FEEDBACK_MESSAGE';
export const FEEDBACK_ERROR = 'ERROR';
export const FEEDBACK_INFO = 'INFO';
export const DISMISS_CARD = 'DISMISS_CARD';
import fetch from 'isomorphic-fetch'


export const dismissFeedbackMessage = (message_id) => {
    return (dispatch, getState) => {
        dispatch({
            type: DISMISS_FEEDBACK_MESSAGE,
            payload: {id: message_id}
        })
    }
};

export const dismissCard = (card_id) => {
    return (dispatch, getState) => {
        dispatch({
            type: DISMISS_CARD,
            payload: {id: card_id}
        })
    }
};

export const dismissAllFeedbackMessage = () => {
    return (dispatch, getState) => {
        dispatch({
            type: DISMISS_ALL_FEEDBACK_MESSAGE
        })
    }
};

export const generateRandomQuote = (category = 'inspire') => {
    return (dispatch, getState) => {
        return fetch(`http://quotes.rest/qod.json?category=${category}`)
            .then(response => response.json())
            .then((json)=> {
                let quote = json.contents.quotes[0].quote;
                let author = json.contents.quotes[0].author;
                dispatch({
                    type: DISPLAY_FEEDBACK_MESSAGE,
                    payload: {
                        id: getState().feedback.length - 1,
                        message: `"${quote}" - ${author}`,
                        type: getState().feedback.length % 2 === 0 ? FEEDBACK_INFO : FEEDBACK_ERROR
                    }
                })
            })
    }
};