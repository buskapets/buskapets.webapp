/**
 * Created by gomes on 03/10/16.
 */
import {LOGIN, LOGOUT, ATTEMPTING_LOGIN, FEEDBACK_ERROR, FEEDBACK_INFO, DISPLAY_FEEDBACK_MESSAGE} from './';
import {Firebase} from '../settings';

export const attemptLogin = (provider) => {
    return (dispatch, getState) => {
        dispatch({type: ATTEMPTING_LOGIN});
        Firebase.auth().signInWithPopup(provider)
            .then((result)=> {
                dispatch({
                    type: DISPLAY_FEEDBACK_MESSAGE,
                    payload: {
                        id: getState().feedback.length,
                        message: `Login with google successful!`,
                        type: FEEDBACK_INFO
                    }
                });
                dispatch({
                    type: LOGIN,
                    payload: {
                        user: result.user,
                        uid: result.uid
                    }
                });
            })
            .catch((error)=> {
                dispatch({type: LOGOUT});
                dispatch({
                    type: DISPLAY_FEEDBACK_MESSAGE,
                    payload: {
                        id: getState().feedback.length - 1,
                        message: `Login with google failed! ${error.messages}`,
                        type: FEEDBACK_ERROR
                    }
                });
            })
    }
};

export const logout = () => {
    return (dispatch, getState) => {
        dispatch({type: LOGOUT});
        Firebase.auth().signOut()
            .catch((error)=> {
                console.error(error);
                dispatch({
                    type: DISPLAY_FEEDBACK_MESSAGE,
                    payload: {
                        id: getState().feedback.length,
                        message: `Logout failed! ${error.messages}`,
                        type: FEEDBACK_ERROR
                    }
                });
            })
    }
};