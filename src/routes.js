/**
 * Created by gomes on 01/10/16.
 */
import React from 'react'
import {Route} from 'react-router'
import App from './containers/App'
import HomePage from './containers/HomePage'

export default <Route path="/" component={App}>
    <Route path="/login" component={App}/>
    <Route path="/home"
           component={HomePage}/>
</Route>
