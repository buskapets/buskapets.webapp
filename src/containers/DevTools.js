/**
 * Created by gomes on 01/10/16.
 */
import React from 'react'
import { createDevTools } from 'redux-devtools'
import LogMonitor from 'redux-devtools-log-monitor'
import DockMonitor from 'redux-devtools-dock-monitor'

export default createDevTools(
    <DockMonitor toggleVisibilityKey="ctrl-alt-l"
                 changePositionKey="ctrl-alt-k">
        <LogMonitor />
    </DockMonitor>
)