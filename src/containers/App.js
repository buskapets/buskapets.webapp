/**
 * Created by gomes on 03/10/16.
 */
import React, {Component, PropTypes} from 'react';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Snackbar from 'material-ui/Snackbar';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FontIcon from 'material-ui/FontIcon';
import {connect} from 'react-redux'
import {
    dismissFeedbackMessage,
    ATTEMPTING_LOGIN,
    generateRandomQuote,
    dismissAllFeedbackMessage,
    dismissCard
} from '../actions'
import {attemptLogin, logout} from '../actions/auth';
import {googleProvider} from '../settings';
import CircularProgress from 'material-ui/CircularProgress';
import styles from '../index.css'
import classNames from 'classnames/bind';
let cx = classNames.bind(styles);

class SingleMessage extends Component {
    static propTypes = {
        feedback: PropTypes.object.isRequired,
        dismissFeedbackMessage: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            open: true,
        };
    }

    handleDismissClick = (e, id) => {
        this.setState({open: false});
        this.props.dismissFeedbackMessage(id);
    };

    render() {
        const feedback = this.props.feedback;
        return (
            <Snackbar
                open={this.state.open}
                message={feedback.message}
                autoHideDuration={4000}
                action={"dismiss"}
                onActionTouchTap={(e)=>this.handleDismissClick(e, feedback.id)}
                onRequestClose={(e)=>this.handleDismissClick(e, feedback.id)}
            />)
    }
}
const mapStateToPropsSM = (state, ownProps) => ({
    messages: state.feedback,
    attempting_login: state.auth.currently
});

export const SM = connect(mapStateToPropsSM, {
    dismissFeedbackMessage
})(SingleMessage);

class SwipeCard extends Component {
    static propTypes = {
        card: PropTypes.object,
        dismissCard: PropTypes.func,
        accept: PropTypes.bool,
        reject: PropTypes.bool,
    };

    defaultProps = {
        accept: false,
        reject: false
    };

    constructor(props) {
        super(props);
    }

    handleCardAccept = (e, card_id) => {
        this.props.dismissCard(card_id);
    };

    handleCardReject = (e, card_id) => {
        this.props.dismissCard(card_id);
    };

    render() {
        const {card} = this.props;
        const style = {
            width: "auto",
            minWidth: "auto",
            maxWidth: "100%",
            height: "100%",
            maxHeight: "30em",
            textAlign: "center"
        };
        let classname = cx({
            stack__card: true,
            stack__card__accept: card.id === undefined,
            stack__card__reject: this.props.reject
        });
        return <li className={classname} key={card.id}><Card
            style={{margin: "0 5%", width: "90%"}}
            id={`card_${card.id}`}>
            <CardHeader
                title={card.title}
                subtitle={card.subtitle}
                avatar={card.userPhoto}
            />
            <CardMedia mediaStyle={style} style={style}
                       overlay={<CardTitle title={card.petName} subtitle={card.petAge}/>}>
                <img src={card.petPhoto} alt={`${card.petName}`} style={style}/>
            </CardMedia>
            <CardActions className="row" style={{textAlign: "center"}}>
                <FloatingActionButton backgroundColor={"#F44336"} zDepth={0}
                                      onClick={(e)=>this.handleCardReject(e, card.id)}>
                    <FontIcon className="material-icons">close</FontIcon>
                </FloatingActionButton>
                <FloatingActionButton backgroundColor={"#9E9E9E"} mini={true} zDepth={0}>
                    <FontIcon className="material-icons">info</FontIcon>
                </FloatingActionButton>
                <FloatingActionButton backgroundColor={"#03A9F4"} zDepth={0}
                                      onClick={(e)=>this.handleCardAccept(e, card.id)}>
                    <FontIcon className="material-icons">check</FontIcon>
                </FloatingActionButton>
            </CardActions>
        </Card></li>
    }

}

class App extends Component {
    static propTypes = {
        messages: PropTypes.array,
        attempting_login: PropTypes.string.isRequired,
        user: PropTypes.object,
        cards: PropTypes.array,
        dismissFeedbackMessage: PropTypes.func.isRequired,
        dismissAllFeedbackMessage: PropTypes.func.isRequired,
        attemptLogin: PropTypes.func.isRequired,
        logout: PropTypes.func.isRequired,
        generateRandomQuote: PropTypes.func,
        dismissCard: PropTypes.func
    };

    handlerRandomQuote = (e) => {
        this.props.generateRandomQuote();
        e.preventDefault();
    };

    handleGoogleButtonClick = (e, provider) => {
        this.props.attemptLogin(provider);
        e.preventDefault();
    };

    handleLogout = (e) => {
        this.props.logout();
        e.preventDefault();
    };

    renderCards() {
        const {cards} = this.props;
        return (cards.map(card=><SwipeCard card={card} dismissCard={this.props.dismissCard}/>))
    };


    renderMessage() {
        const {messages} = this.props;
        if (!messages && messages.length === 0) {
            return null
        }
        return (<div>
            {
                messages.map(
                    (feedback) => (<SM key={feedback.id + (new Date()).getTime()} feedback={feedback}/>)
                )
            }
        </div>);
    }

    renderNoMoreCards() {
        const style = {
            width: "auto",
            minWidth: "auto",
            maxWidth: "100%",
            height: "100%",
            maxHeight: "30em",
            textAlign: "center"
        };
        return (<li className="stack__card"><Card style={{margin: "0 5%", width: "90%"}} key={0}
                                                  id={`card_${0}`}>
            <CardHeader
                title="No more pets =/"
                subtitle="Sorry..."/>
            <CardMedia mediaStyle={style} style={style}>
                <img src="https://s-media-cache-ak0.pinimg.com/originals/d8/94/63/d894637203aa8193e928c13c2a83ffdc.jpg"
                     alt="no+pets"
                     style={style}/>
            </CardMedia>
            {/*<CardActions className="row" style={{textAlign: "center"}}>*/}
            {/*<FloatingActionButton backgroundColor={"#F44336"} zDepth={0}*/}
            {/*onClick={(e)=>this.handleCardDismiss(e, card.id)}>*/}
            {/*<FontIcon className="material-icons">close</FontIcon>*/}
            {/*</FloatingActionButton>*/}
            {/*<FloatingActionButton backgroundColor={"#9E9E9E"} mini={true} zDepth={0}>*/}
            {/*<FontIcon className="material-icons">info</FontIcon>*/}
            {/*</FloatingActionButton>*/}
            {/*<FloatingActionButton backgroundColor={"#03A9F4"} zDepth={0}*/}
            {/*onClick={(e)=>this.handleCardDismiss(e, card.id)}>*/}
            {/*<FontIcon className="material-icons">check</FontIcon>*/}
            {/*</FloatingActionButton>*/}
            {/*</CardActions>*/}
        </Card></li>)
    }

    render() {
        let logging_in = this.props.attempting_login === ATTEMPTING_LOGIN;
        let logged_in = this.props.user != null && this.props.user !== undefined;
        const {cards} = this.props;

        return (
            <div>
                <AppBar title="BuskaPets" style={{marginBottom: "1em"}}/>
                <div className="container">
                    <div className="row">
                        <RaisedButton secondary={true} className={"twelve columns"}
                                      onClick={(e)=>this.handleGoogleButtonClick(e, googleProvider)}
                                      style={{display: (logged_in || logging_in) ? 'none' : 'block'}}
                                      label="Google Login"/>
                    </div>
                    <div className="row">
                        <RaisedButton onClick={(e)=>this.handleLogout(e)} className={"six columns"}
                                      style={{display: (!logged_in || logging_in) ? 'none' : 'block'}}
                                      label={"Logout"}/>
                        <RaisedButton onClick={(e)=>this.handlerRandomQuote(e)} className={"six columns"}
                                      style={{display: (!logged_in || logging_in) ? 'none' : 'block'}}
                                      label={"Random quote!"}/>
                    </div>
                    <div className="row">
                        <ul className="stack">
                            {this.renderNoMoreCards()}
                            {this.renderCards()}
                        </ul>
                    </div>
                    {this.renderMessage()}
                    <div style={{
                        display: logging_in ? 'block' : 'none',
                        textAlign: "center",
                        position: "fixed",
                        bottom: "0",
                        right: "0"
                    }}>
                        <CircularProgress />
                    </div>
                </div>
                <footer>
                    <div className="container" style={{marginTop: "1em"}}>
                        <div style={{textAlign: "center"}}>
                            <p>
                                <strong>BuskaPets</strong> by <a href="http://utyl.com.br">Utyl</a>
                            </p>
                        </div>
                    </div>
                </footer>
            </div>
        )
    }
}

const
    mapStateToProps = (state, ownProps) => ({
        messages: state.feedback,
        attempting_login: state.auth.currently,
        user: state.auth.user,
        cards: state.pets
    });

export default connect(mapStateToProps, {
    dismissFeedbackMessage, attemptLogin, logout, generateRandomQuote, dismissAllFeedbackMessage, dismissCard
})(App)