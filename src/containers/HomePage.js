/**
 * Created by gomes on 04/10/16.
 */
import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux'


class HomePage extends Component {
    static propTypes = {
        children: PropTypes.node
    };

    render() {
        const {children} = this.props;
        return (
            <div>
                <p>User logged in!</p>
                {children}
            </div>
        )
    }
}
const mapStateToProps = (state, ownProps) => ({});

export default connect(mapStateToProps, {})(HomePage)