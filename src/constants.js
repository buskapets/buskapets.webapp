/**
 * Created by gomes on 03/10/16.
 */
export const ANONYMOUS = 'ANONYMOUS';
export const LOGGED_IN = 'LOGGED_IN';
export const USER_MODEL = {
    type: [],
    email: "",
    first_name: "",
    last_name: "",
    phone: "",
    uid: "",
    adopted: [],
    pets: []
};
export const PETS_MODEL = {
    type: "",
    name: "",
    size: "",
    details: ""
};