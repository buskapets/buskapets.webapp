/**
 * Created by gomes on 01/10/16.
 */
import React from 'react'
import {render} from 'react-dom'
import {browserHistory} from 'react-router'
import {syncHistoryWithStore} from 'react-router-redux'
import Root from './containers/Root.prod'
import configureStore from './store/configureStore'
import injectTapEventPlugin from 'react-tap-event-plugin';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

// store.subscribe(()=>{
//     console.log(store.getState());
// });

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

render(
    <Root store={store} history={history}/>,
    document.getElementById('root')
);