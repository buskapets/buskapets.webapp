/**
 * Created by gomes on 03/10/16.
 */
import {
    cyan500, cyan700,
    pinkA200,
    grey100, grey300, grey400, grey500,
    white, darkBlack, fullBlack,
} from 'material-ui/styles/colors';
import {fade} from 'material-ui/utils/colorManipulator';
import spacing from 'material-ui/styles/spacing';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import {initializeApp, auth} from 'firebase';

const FIREBASE_CONFIG = {
    apiKey: "AIzaSyDLDSbImu46aQQsxs7M8YYB6UsseT3Z2dU",
    authDomain: "backend-example.firebaseapp.com",
    databaseURL: "https://backend-example.firebaseio.com",
    storageBucket: "backend-example.appspot.com",
    messagingSenderId: "767591657518"
};
export const Firebase = initializeApp(FIREBASE_CONFIG);

// firebase providers
export const googleProvider = new auth.GoogleAuthProvider();

// theme settings
export const theme = getMuiTheme({
    spacing: spacing,
    fontFamily: 'Roboto, sans-serif',
    palette: {
        primary1Color: cyan500,
        primary2Color: cyan700,
        primary3Color: grey400,
        accent1Color: pinkA200,
        accent2Color: grey100,
        accent3Color: grey500,
        textColor: darkBlack,
        alternateTextColor: white,
        canvasColor: white,
        borderColor: grey300,
        disabledColor: fade(darkBlack, 0.3),
        pickerHeaderColor: cyan500,
        clockCircleColor: fade(darkBlack, 0.07),
        shadowColor: fullBlack,
    },
});