/**
 * Created by gomes on 03/10/16.
 */
import {routerReducer as routing} from 'react-router-redux';
import {combineReducers} from 'redux';
import {ANONYMOUS, LOGGED_IN} from '../constants';
import {
    LOGIN,
    LOGOUT,
    ATTEMPTING_LOGIN,
    DISMISS_FEEDBACK_MESSAGE,
    DISPLAY_FEEDBACK_MESSAGE,
    DISMISS_ALL_FEEDBACK_MESSAGE,
    DISMISS_CARD
} from '../actions';

const initialStore = {
    feedback: [],
    auth: {
        currently: ANONYMOUS,
        user: null,
        uid: null,
    },
    pets: [
        {
            id: 1,
            title: "Card title",
            subtitle: "Card subtitle",
            userPhoto: "https://randomuser.me/api/portraits/women/9.jpg",
            petName: "Batata",
            petAge: "6 meses",
            petPhoto: "https://placekitten.com/600/500"
        },
        {
            id: 2,
            title: "Card title",
            subtitle: "Card subtitle",
            userPhoto: "https://randomuser.me/api/portraits/women/9.jpg",
            petName: "Batata",
            petAge: "6 meses",
            petPhoto: "https://placekitten.com/500/500"
        },
        {
            id: 3,
            title: "Card title",
            subtitle: "Card subtitle",
            userPhoto: "https://randomuser.me/api/portraits/women/9.jpg",
            petName: "Batata",
            petAge: "6 meses",
            petPhoto: "https://placekitten.com/600/600"
        }]
};

const pets = (state = initialStore.pets, action) => {
    switch (action.type) {
        case DISMISS_CARD:
            return state.map((i, n) => {
                if (i.id === action.payload.id) {
                    i.id = undefined;
                }
                return i;
            });
        default:
            return state;
    }
};

const feedback = (state = initialStore.feedback, action) => {
    switch (action.type) {
        case DISMISS_FEEDBACK_MESSAGE:
            return state.filter((i, n) => i.id !== action.payload.id);
        case DISPLAY_FEEDBACK_MESSAGE:
            return state.concat({
                id: action.payload.id,
                message: action.payload.message,
                type: action.payload.type
            });
        case DISMISS_ALL_FEEDBACK_MESSAGE:
            return [];
        default:
            return state;
    }
};

const auth = (state = initialStore.auth, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                currently: LOGGED_IN,
                user: action.payload.user,
                uid: action.payload.uid
            };
        case LOGOUT:
            return {
                currently: ANONYMOUS,
                user: null,
                uid: null
            };
        case ATTEMPTING_LOGIN:
            return {
                currently: ATTEMPTING_LOGIN
            };
        default:
            return state
    }
};


const rootReducer = combineReducers({
    pets,
    auth,
    feedback,
    routing
});

export default rootReducer

